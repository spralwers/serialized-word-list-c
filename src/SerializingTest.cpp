#include "SerializingTest.h"
#include "catch.hpp"
#include "iostream"
using namespace std;
SerializingTest::SerializingTest()
{
    stringNodeList = new Node<string>[3];
    stringNodeList[0].setElement("5");
    stringNodeList[1].setElement("Hello");
    stringNodeList[2].setElement("byE");
}

SerializingTest::~SerializingTest()
{

    delete[] stringNodeList;
}

void SerializingTest::TestNode()

{
    //check if nodes were properly initialized
    cout << "Node 0 is '5': ";
    if (stringNodeList[0].getElement().compare("5") == 0)
        cout << "true" << endl;

    //check if getNext works.
    stringNodeList[0].setNext(&stringNodeList[1]);
    //check if the addresses are the same AND if the underlying content is as expected
    if (stringNodeList[0].getNext() == &stringNodeList[1] && stringNodeList[0].getNext()->getElement().compare("Hello") == 0)
        cout << "set and get next work." << endl;
}

void SerializingTest::TestLL()
{
    LinkedList<string> testlist;
    for (int i=0; i<3; i++)
        testlist.add(stringNodeList[i].getElement());

    LLIterator<string> llIter(testList);
    while (llIter.ptr->hasNext())
    {
        cout << llIter.ptr->getElement();
        llIter.advance();
    }

}

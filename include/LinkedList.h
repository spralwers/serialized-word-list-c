#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#include <iterator>

template <class T>
class Node
{
    T content;
    Node* next;
    bool hasnext;
    bool hascontent;
    friend class SerializingTest;

    public:

    Node()
    {
        hasnext=false;
        hascontent=false;
    }

    Node(T c)
    {
        content = c;
        hascontent = true;
    }

    T getElement()
    {
        return content;
    }
    void setElement(T c)
    {
        content = c;
        hascontent=true;
    }

    void setNext(Node* nextNode)
    {
        next = nextNode;
        hasnext = true;
    }

    Node* getNext()
    {
        return next;
    }

    bool hasNext()
    {
        return hasnext;
    }

};

template <class T>
class LinkedList
{
        protected:
    private:
        Node<T> front;
        int size;
    public:
        LinkedList()
        {
            size = 0;
        }
        LinkedList(T frontElement)
        {
            front.setElement(frontElement);
        }

       void add(T newEle)
        {
            Node<T> *nextNode;
            if (size ==0)
            {
               front.setElement(newEle);
               size++;
               return;
            }

            Node<T> *ptr = &front;
            while (ptr->hasNext())
                ptr=ptr->getNext();
            ptr->setNext(nextNode);
            size++;
        }

        T getFront()
        {
            return front.getElement();
        }

        int getSize()
        {
            return size;
        }

};

//wrote this literally to practice trying out "interfaces" in C++
class Iterator {

public:
   virtual void begin() = 0;
   virtual void end() = 0;
   virtual void advance() = 0;

};

//this iterator is meant only to advance and read from a linked list.
template <class T>
class LLIterator : public Iterator
{
public:
    Node<T> *ptr;
    LinkedList<T> listToIterate;
    LLIterator()
    {

    }
    LLIterator(LinkedList<T> listToIterate)
    {
    this.listToIterate = listToIterate;
    ptr = &(this.listToIterate.front());
    }

    void begin()
    {
        ptr = &listToIterate.front();
    }

    void advance()
    {
        ptr = ptr.getNext();
    }

};

#endif // LINKEDLIST_H

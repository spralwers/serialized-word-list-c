#ifndef SERIALIZINGTEST_H
#define SERIALIZINGTEST_H
#include "LinkedList.h"
#include <string>

//Used to test every element of the program.
class SerializingTest
{
    public:
        SerializingTest();
        ~SerializingTest();
        void TestNode();
        void TestLL();
        Node<std::string> *stringNodeList;
        int foo[];
    protected:
    private:
};

#endif // SERIALIZINGTEST_H
